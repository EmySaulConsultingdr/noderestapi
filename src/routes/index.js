const express = require('express')
const Router = require('router')
const cors = require('cors')
const helmet = require('helmet')
const compression = require('compression')
const { ErrorMiddleware, NotFoundMiddleware } = require('../middlewares')
const swaggerUI = require('swagger-ui-express')
const { SWAGGER_PATH } = require('../config')
const swaggerDocument = require(SWAGGER_PATH)

module.exports = function ({ HomeRoutes, UserRoutes, CommentRoutes, IdeaRoutes }) {
    const router = Router();
    const apiRoutes = Router();

    apiRoutes
        .use(express.json())
        .use(cors())
        .use(helmet())
        .use(compression())

    apiRoutes.use('/home', HomeRoutes)
    apiRoutes.use('/user', UserRoutes)
    apiRoutes.use('/idea', IdeaRoutes)
    apiRoutes.use('/comment', CommentRoutes)
    router.use('/v1/api/', apiRoutes)
    router.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument))


    router.use(NotFoundMiddleware)
    router.use(ErrorMiddleware)
    
    return router
}