const { Router } = require('express')
const { CacheMiddleware } = require('../middlewares')
const { CACHE_TIME } = require('../helpers')
module.exports = function ({ CommentController }) {
    const router = Router();

    router.get('/:commentId/unique', [CacheMiddleware(CACHE_TIME.ONE_HOUR)], CommentController.get)
    router.get('/:ideaId', [CacheMiddleware(CACHE_TIME.ONE_HOUR)], CommentController.getIdeaComments)
    router.post('/:ideaId', CommentController.createComment)
    router.patch('/:commentId', CommentController.update)
    router.delete('/:commentId', CommentController.delete)
    return router
}