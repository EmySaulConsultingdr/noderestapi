module.exports = (req, res, next) => {
    console.log('res.status', res.status)
    return res.status(404).send({ status: 404, message: 'Resource not found' })
}